---
title: "R Lopputyö"
author: "hlrimm"
output:
  pdf_document: default
  html_document:
    code_folding: show
    number_sections: yes
  word_document: default
---
## Harjoitustyö 
Tein harjoitustyön jo kotitehtävissä käytetystä aineistosta (FAO), sillä omat R:n käsittelytaidot huomioon ottaen, koin, että se sopisi paremmin omalle taitotasolleni.
Aineisto löytyy osoiteesta: http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip

library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))
```
#Ladataan myös rmarkdown -paketti
library(rmarkdown)

#Datan tuominen R:ään ja tallentaminen
dir.create("./aineisto")


fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))

download.file(url = "http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS" ,destfile = "./aineisto/fao.csv")

#Aloitetaan datan tarkasteleminen
library(tidyr)
library(dplyr)
d <- fao
table(d$Country)
table(d$Year)
table(d$Item)
table(d$Element)

#Kuinka paljon Pohjoismaissa tuotettiin perunoita (Potatoes), tomaatteja (Tomatoes) ja porkkanoita ja nauriita (Carrots and turnips) vuosina 2000-2005?

#Luodaan objekti *Vihannekset*
d <- filter(Country == c("Finland", "Sweden", "Norway", "Denmark", "Iceland"),
            Element == "Production",
            Item == c("Potatoes", "Tomatoes", "Carrots and turnips"),
            Unit == "Tonnes",
            Year == 2000:2005)
#Valitaan *Year*, *Country*, *Item*, *Value*           
    Select(Year, Country, Item, Value)   -> vihannekset      

#Tämän jälkeen piirretään näistä kuviot
library(ggplot2)

ggplot(vihannekset, aes(x=Item,y=Value)) + geom_bar(stat="identity")

ggplot(vihannekset, aes(x=Item,y=Value)) + geom_bar(stat="identity") +
  facet_wrap(~Country)
  
#Piirretään maittaiset viivakuviot perunan tuotannosta sekä viljelyalasta ja valitaan vuodet 2000-2005.    
d <- filter(Country == c("Finland","Sweden","Norway","Denmark", "Iceland"),
             Element == "Production",
             Item == "Potatoes",
             Unit == "tonnes",
             Year == "2000:2005"
) 
  select(Country,Year,Value,Item) -> peruna_tuotanto
  ggplot(peruna_tuotanto, aes(x=Year,y=Value,color=Country)) + geom_line()
  

d <- filter(Country == c("Finland","Sweden","Norway","Denmark", "Iceland"),
             Element == "Area harvested",
             Item == "Potatoes",
             Unit == "Ha",
             Year == 2000:2005
)
  select(Country,Year,Value,Item) -> peruna_viljelysala
 ggplot(peruna_viljelysala, aes(x=Year,y=Value,color=Country)) + geom_line()  
  
#Tämän jälkeen tallennetaan kuviot
dir.create("./aineisto/kuviot")

tolppakuvio1 <- ggplot(vihannekset, aes(x=Item,y=Value)) + geom_bar(stat="identity")
ggsave(plot=tolppakuvio1, filename = "tolppakuvio1.png", path="./aineisto/kuviot")

tolppakuvio2 <-  ggplot(vihannekset, aes(x=Item,y=Value)) + geom_bar(stat="identity") +
  facet_wrap(~Country) 
ggsave(plot=tolppakuvio2, filename = "tolppakuvio2.png", path="./aineisto/kuviot")

tuotanto <- ggplot(dat, aes(x=Year,y=Value,color=Country)) + geom_line()
ggsave(plot=tuotanto, filename = "tuotanto.png", path="./aineisto/kuviot")

viljelysala <- ggplot(dat, aes(x=Year,y=Value,color=Country)) + geom_line()  
ggsave(plot=viljelysala, filename = "viljelysala.png", path="./aineisto/kuviot")



